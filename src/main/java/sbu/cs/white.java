package sbu.cs;

public class white {
    //
    private static String func1(String firsStr , String secStr){

        int size=0;
        if(firsStr.length() >= secStr.length()){
            size = secStr.length();
        }
        else{
            size = firsStr.length();
        }

        StringBuilder sb = new StringBuilder();
        for(int i=0 ; i<size ; i++){
            sb.append(firsStr.charAt(i)).append(secStr.charAt(i));
        }
        int n=0;
        int index=0;
        if(firsStr.length() != secStr.length()){
            if(firsStr.length() > secStr.length()){
                index = size;
                for(int j=index ; j<firsStr.length(); j++){
                    sb.append(firsStr.charAt(j));
                }
            }
            if(firsStr.length() < secStr.length()){
                index = size;
                for(int j=index ; j<secStr.length(); j++){
                    sb.append(secStr.charAt(j));
                }
            }
        }
        return sb.toString();
    }

    private static String func2(String firsStr , String secStr){
        StringBuilder reversedSecStr = new StringBuilder();
        for(int i=secStr.length() -1 ; i>=0 ; i--){
            reversedSecStr.append(secStr.charAt(i));
        }
        StringBuilder ans = new StringBuilder();
        for(int j=0 ; j<firsStr.length() ; j++){
            ans.append(firsStr.charAt(j));
        }
        for(int k=0 ; k<reversedSecStr.length() ; k++){
            ans.append(reversedSecStr.charAt(k));
        }

        return ans.toString();
    }

    private static String func3(String firsStr , String secStr){
        StringBuilder reversedSecStr = new StringBuilder();
        for(int i=secStr.length() -1 ; i>=0 ; i--){
            reversedSecStr.append(secStr.charAt(i));
        }

        int size=0;
        if(firsStr.length() >= reversedSecStr.length()){
            size = secStr.length();
        }
        else{
            size = firsStr.length();
        }

        StringBuilder sb = new StringBuilder();
        for(int i=0 ; i<size ; i++){
            sb.append(firsStr.charAt(i)).append(reversedSecStr.charAt(i));
        }

        int index=0;
        if(firsStr.length() != reversedSecStr.length()) {
            if (firsStr.length() > reversedSecStr.length()) {
                index = size;
                for (int j = index; j < firsStr.length(); j++) {
                    sb.append(firsStr.charAt(j));
                }
            }
            if (firsStr.length() < reversedSecStr.length()) {
                index = size;
                for (int j = index; j < reversedSecStr.length(); j++) {
                    sb.append(reversedSecStr.charAt(j));
                }
            }
        }
        return sb.toString();
    }

    private static String func4(String firsStr , String secStr){
        if((firsStr.length()%2)==0){
            return firsStr;
        }
        else{
            return secStr;
        }
    }

    private static String func5(String firsStr , String secStr)
    {
        String newStr = "";
        int index = Math.min(firsStr.length(), secStr.length());

        for(int i = 0; i < index; i++)
        {
            char c = (char) (((firsStr.charAt(i) - 97 + secStr.charAt(i) - 97) % 26) + 97);
            newStr += c;
        }

        return newStr + firsStr.substring(index) + secStr.substring(index);
        /*StringBuilder sb = new StringBuilder();
        if(firsStr.length() <= secStr.length()){
            for(int i=0 ; i<firsStr.length() ; i++){
                int num = (firsStr.charAt(i) + secStr.charAt(i))%26;
                char Char = (char)num;
                sb.append(Char);
            }
        }
        if(firsStr.length() > secStr.length()){
            for(int i=0 ; i<secStr.length() ; i++){
                int num = (firsStr.charAt(i) + secStr.charAt(i))%26;
                char Char = (char)num;
                sb.append(Char);
            }
        }
        int index=0;
        if(firsStr.length() != secStr.length()) {
            if (firsStr.length() > secStr.length()) {
                index = secStr.length();
                for (int j = index; j < firsStr.length(); j++) {
                    sb.append(firsStr.charAt(j));
                }
            }
            if (firsStr.length() < secStr.length()) {
                index = firsStr.length();
                for (int j = index; j < secStr.length(); j++) {
                    sb.append(secStr.charAt(j));
                }
            }
        }
        return sb.toString();*/
    }

    public static String whichFunc(String firsStr , String secStr , int num){

        String str = "";
        switch (num){
            case 1:
                str=func1(firsStr,secStr);
                break;
            case 2:
                str=func2(firsStr,secStr);
                break;
            case 3:
                str=func3(firsStr,secStr);
                break;
            case 4:
                str=func4(firsStr,secStr);
                break;
            case 5:
                str=func5(firsStr,secStr);
                break;
        }
        return str;
    }

}
