package sbu.cs;

public class yellowSquare extends bigSquare {
    //
    private String input;
    private String output;

    public yellowSquare(int num) {
        super(num);
    }

    public String getOutput() {
        //output=black.whichFunc(input,super.num);
        return output;
    }

    public void setInput(String input) {
        this.input = input;
        this.output = black.whichFunc(input, super.getNum());
    }

    public void setOutput(String output) {
        this.output = output;
    }
}
