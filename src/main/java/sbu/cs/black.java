package sbu.cs;

public class black {
    //
    private static String func1(String str){
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();
        return sb.toString();
    }

    private static String func2(String str){
        /*int size = str.length();
        StringBuilder sb = new StringBuilder(2*size);
        for(int i=0 ; i<size ; i++){
            sb.append(str.indexOf(i)).append(str.indexOf(i));
        }
        return sb.toString();*/
        String newStr = "";

        for (char c : str.toCharArray())
        {
            newStr += c + "" + c;
        }

        return newStr;
    }

    private static String func3(String str){
        int size = str.length();
        StringBuilder sb = new StringBuilder(2*size);
        for(int i=0 ; i<size ; i++){
            sb.append(str.indexOf(i));
        }
        for(int i=0 ; i<size ; i++){
            sb.append(str.indexOf(i));
        }
        return sb.toString();
    }

    private static String func4(String str){
        int size = str.length();
        StringBuilder sb = new StringBuilder();
        sb.append(str.charAt(size-1));
        for(int i=0 ; i<size-1 ; i++){
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }

    private static String func5(String str){
        String newStr = "";

        for (char c : str.toCharArray())
        {
            newStr += (char)('z' - (c - 'a'));
        }

        return newStr;
    }

    public static String whichFunc(String str , int num){

        String string = "";
        switch (num){
            case 1:
                string=func1(str);
                break;
            case 2:
                string=func2(str);
                break;
            case 3:
                string=func3(str);
                break;
            case 4:
                string=func4(str);
                break;
            case 5:
                string=func5(str);
                break;
        }
        return string;
    }

}
