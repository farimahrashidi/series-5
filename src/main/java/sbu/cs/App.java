package sbu.cs;

public class App {
//
    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    private int n;
    private bigSquare[][] squares;
    private int [][] arr;

    public String main(int n, int[][] arr, String input) {

        this.n = n;
        squares = new bigSquare[n][n];
        this.arr = arr;

        firstRowColors();
        middleRowsColor();
        lastRowColors();

        ((greenSquare)squares[0][0]).setInp(input);

        String ans = ans();

        return ans;
    }

    private String ans()
    {
        doFirstRow();
        doMiddleRow();
        doLastRow();

        return ((pinkSquare)squares[n - 1][n - 1]).getOutput();
    }

    private void firstRowColors() {

        for(int i = 0; i < n - 1; i++) {
            squares[0][i] = new greenSquare(arr[0][i]);
        }

        squares[0][n - 1] = new yellowSquare(arr[0][n - 1]);
    }

    private void middleRowsColor() {

        for(int i = 1; i < n - 1; i++) {

            for(int j = 0; j < n; j++) {

                if(j == 0) {
                    squares[i][j] = new greenSquare(arr[i][j]);
                    continue;
                }

                else if(j == n -  1) {
                    squares[i][j] = new pinkSquare(arr[i][j]);
                    continue;
                }

                squares[i][j] = new blueSquare(arr[i][j]);
            }
        }
    }


    private void lastRowColors() {

        squares[n - 1][0] = new yellowSquare(arr[n - 1][0]);

        for(int i = 1; i < n; i++) {

            squares[n - 1][i] = new pinkSquare(arr[n - 1][i]);
        }
    }


    private void doFirstRow() {

        for(int i = 0; i < n; i++) {

            if(i == 0) {

                String outPut = ((greenSquare)squares[0][0]).getOutput();

                ((greenSquare)squares[0][i + 1]).setInp(outPut);

                ((greenSquare)squares[1][i]).setInp(outPut);

                continue;
            }

            else if(i == n - 1) {

                String out = ((yellowSquare)squares[0][i]).getOutput();

                ((pinkSquare)squares[1][i]).setUpInp(out);

                continue;
            }


            String out = ((greenSquare)squares[0][i]).getOutput();
            if(squares[0][i + 1] instanceof greenSquare) {

                ((greenSquare)squares[0][i + 1]).setInp(out);
            }

            else {
                ((yellowSquare)squares[0][i + 1]).setInput(out);
            }

            ((blueSquare)squares[1][i]).setUpInp(out);

        }
    }

    private void doMiddleRow() {

        for(int i = 1; i < n - 1; i++) {

            for(int j = 0; j < n; j++) {

                if(j == 0) {

                    String outp = ((greenSquare)squares[i][j]).getOutput();

                    ((blueSquare)squares[i][j + 1]).setLeftInp(outp);

                    if(squares[i + 1][j] instanceof greenSquare) {

                        ((greenSquare)squares[i + 1][j]).setInp(outp);
                    }

                    else {

                        ((yellowSquare)squares[i + 1][j]).setInput(outp);
                    }
                }

                else if(j == n - 1) {

                    String out = ((pinkSquare)squares[i][j]).getOutput();

                    ((pinkSquare)squares[i + 1][j]).setUpInp(out);
                }

                else {

                    String rightOutput = ((blueSquare)squares[i][j]).getRightOutput();
                    String downOutput = ((blueSquare)squares[i][j]).getDownOutput();

                    if(squares[i][j + 1] instanceof blueSquare) {

                        ((blueSquare)squares[i][j + 1]).setLeftInp(rightOutput);
                    }
                    else if(squares[i][j + 1] instanceof pinkSquare) {

                        ((pinkSquare)squares[i][j + 1]).setLeftInp(rightOutput);
                    }

                    if(squares[i + 1][j] instanceof blueSquare) {

                        ((blueSquare)squares[i + 1][j]).setUpInp(downOutput);
                    }
                    else if(squares[i + 1][j] instanceof pinkSquare) {

                        ((pinkSquare)squares[i + 1][j]).setUpInp(downOutput);
                    }

                }
            }
        }
    }


    private void doLastRow()
    {
        for(int i = 0; i < n - 1; i++)
        {
            if(i == 0)
            {
                String out = ((yellowSquare)squares[n - 1][i]).getOutput();

                ((pinkSquare)squares[n - 1][i + 1]).setLeftInp(out);

                continue;
            }

            String out = ((pinkSquare)squares[n - 1][i]).getOutput();

            ((pinkSquare)squares[n - 1][i + 1]).setLeftInp(out);
        }
    }


}
