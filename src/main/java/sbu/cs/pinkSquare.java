package sbu.cs;

public class pinkSquare extends bigSquare{
    //
    private String upInp="";
    private String leftInp="";
    private String output;

    public pinkSquare(int num) {
        super(num);
    }

    public String getOutput() {

        //output = white.whichFunc(leftInp,upInp,super.num);
        return output;
    }

    public void setUpInp(String upInp) {
        this.upInp = upInp;
        doIt();
    }

    public void setLeftInp(String leftInp) {
        this.leftInp = leftInp;
        doIt();
    }

    private void doIt()
    {
        if(this.leftInp.length() != 0 && this.upInp.length() != 0)
        {
            this.output = white.whichFunc(leftInp, upInp, super.getNum());
        }
    }
}
