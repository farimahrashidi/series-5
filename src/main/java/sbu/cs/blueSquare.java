package sbu.cs;

public class blueSquare extends bigSquare{
    //
    private String upInp;
    private String leftInp;
    private String rightOutput;
    private String downOutput;

    public blueSquare(int num) {
        super(num);
    }

    public String getRightOutput() {
        //rightOutput=black.whichFunc(leftInp,super.num);
        return rightOutput;
    }

    public String getDownOutput() {
        //downOutput=black.whichFunc(upInp,super.num);
        return downOutput;
    }

    public void setUpInp(String upInp) {
        this.upInp = upInp;
        this.downOutput = black.whichFunc(upInp, super.getNum());

    }

    public void setLeftInp(String leftInp) {
        this.leftInp = leftInp;
        this.rightOutput = black.whichFunc(leftInp, super.getNum());

    }
}
